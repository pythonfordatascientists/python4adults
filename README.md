
This is the repo for my Python For Data Scientists class.
Students in my class are mainly professionals with statistics/math background.
The goal of this class is to get the students started with Python programming.

Leaning by doing is my teaching phylosophy. Hands-on projects will be used throughout the class.

Topics that will be covered in this class: 
  * Python/Jupyter Notebook/Annaconda installation
  * Python basics: variables, functions, loops, conditions, classes, exceptions
  * Python data structures: list, set, tuple, dictionary
  * Numpy and Pandas: consume data in common data formats: JSON, CSV, SQL, Text, Excel
  * Matplotlib and Seaborn: data visualization
  * Machine Learning with Scikit Learn
    * Linear Regression
    * Decision Tree
    * Logistic Regression
    * Random Forest
    

You're free to use or modify the material.
